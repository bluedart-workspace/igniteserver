package com.capiot.igniteserver.processors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.spi.discovery.tcp.TcpDiscoverySpi;
import org.apache.ignite.spi.discovery.tcp.ipfinder.vm.TcpDiscoveryVmIpFinder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Arrays;


@Component
public class CreateIgniteClusterServer implements Processor {

    private static final Logger log = LoggerFactory.getLogger(CreateIgniteClusterServer.class);

    public CreateIgniteClusterServer() {
    }

    private static IgniteCache<String, String> cache;
    CacheConfiguration cachecfg = new CacheConfiguration();

    private static Ignite ignite;

    @Value("${ipList}")
    private String iplist;


    @Override
    public void process(Exchange exchange) throws Exception {

        String[] clusterIpList = iplist.split(",");

        TcpDiscoverySpi spi = new TcpDiscoverySpi();
        TcpDiscoveryVmIpFinder tcpDiscoveryVmIpFinder = new TcpDiscoveryVmIpFinder();
        tcpDiscoveryVmIpFinder.setAddresses(Arrays.asList(clusterIpList));
        spi.setIpFinder(tcpDiscoveryVmIpFinder);
        IgniteConfiguration igniteConfiguration = new IgniteConfiguration();
        igniteConfiguration.setDiscoverySpi(spi);
        igniteConfiguration.setIgniteInstanceName("IgniteServer");
        igniteConfiguration.setMetricsLogFrequency(3600000L);
//        DataStorageConfiguration storageConfiguration = new DataStorageConfiguration();
//        storageConfiguration.getDefaultDataRegionConfiguration().setPersistenceEnabled(true);
//        igniteConfiguration.setDataStorageConfiguration(storageConfiguration);
        Ignite ignite = Ignition.start(igniteConfiguration);
    }
}
