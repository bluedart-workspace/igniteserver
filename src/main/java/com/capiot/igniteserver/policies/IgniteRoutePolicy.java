package com.capiot.igniteserver.policies;

import org.apache.camel.Route;
import org.apache.camel.support.RoutePolicySupport;
import org.apache.ignite.Ignition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class IgniteRoutePolicy extends RoutePolicySupport {

    private static final Logger log = LoggerFactory.getLogger(IgniteRoutePolicy.class);

    @Override
    public void onStop(Route route) {
        log.info("Stopping Ignite Cluster server .....");

        Ignition.stop("igniteserver", true);
        super.onStop(route);
    }
}
