package com.capiot.igniteserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class IgniteserverApplication extends SpringBootServletInitializer {

//    public static void main(String[] args) {
//        SpringApplication.run(IgniteserverApplication.class, args);
//
//    }
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder
                                                         application) {
        return application.sources(IgniteserverApplication.class);
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(IgniteserverApplication.class, args);
    }

}
