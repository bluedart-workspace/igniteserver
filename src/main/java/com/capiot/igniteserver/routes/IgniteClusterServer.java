package com.capiot.igniteserver.routes;

import com.capiot.igniteserver.policies.IgniteRoutePolicy;
import com.capiot.igniteserver.processors.CreateIgniteClusterServer;
import org.apache.camel.builder.RouteBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class IgniteClusterServer extends RouteBuilder {

    private static final Logger logger = LoggerFactory.getLogger(IgniteClusterServer.class);

    @Value("${ipList}")
    private String iplist;

    public IgniteClusterServer() {
    }

    private IgniteRoutePolicy igniteRoutePolicy = new IgniteRoutePolicy();

    @Override
    public void configure() throws Exception {

        from("timer://IgniteClusterServer?repeatCount=1&delay=500")
                .bean(CreateIgniteClusterServer.class, "process")
                .log("Ignite Server Started")
        ;
    }
}
